import list.ordenacao.OrdenacaoPessoa;

public class Main {

    public static void main(String[] args) {

        OrdenacaoPessoa ordenacaoPessoa = new OrdenacaoPessoa();
        ordenacaoPessoa.adicionarPessoa("Nome 1", 20, 1.56);
        ordenacaoPessoa.adicionarPessoa("Nome 2", 30, 1.80);
        ordenacaoPessoa.adicionarPessoa("Nome 3", 25, 1.70);
        ordenacaoPessoa.adicionarPessoa("Nome 4", 17, 1.56);

        System.out.println(ordenacaoPessoa.ordenarPorIdade());
        System.out.println(ordenacaoPessoa.ordenarPorAltura());

        // Daqui para baixo é o teste do CatalogoLivros
//        CatalogoLivros catalogoLivros = new CatalogoLivros();
//        catalogoLivros.adicionarLivro("Livro 1", "Autor 1", 2020);
//        catalogoLivros.adicionarLivro("Livro 1", "Autor 2", 2021);
//        catalogoLivros.adicionarLivro("Livro 2", "Autor 2", 2022);
//        catalogoLivros.adicionarLivro("Livro 3", "Autor 3", 2023);
//        catalogoLivros.adicionarLivro("Livro 4", "Autor 4", 1994);
//
//        System.out.println(catalogoLivros.pesquisarPorAutor("Autor 2"));
//        System.out.println(catalogoLivros.pesquisarPorIntervalosAnos(2020, 2022));
//        System.out.println(catalogoLivros.pesquisarPorTitulo("Livro 1"));

        // Daqui para baixo é o teste do ListaTarefa

//        ListaTarefa listaTarefa = new ListaTarefa();
//
//        System.out.printf("O número total de elementos na lista é: %d%n", listaTarefa.obterNumeroTotalTarefa());
//
//        listaTarefa.adicionarTarefa("Tarefa 1");
//        listaTarefa.adicionarTarefa("Tarefa 1");
//        listaTarefa.adicionarTarefa("Tarefa 3");
//        System.out.printf("O número total de elementos na lista é: %d%n", listaTarefa.obterNumeroTotalTarefa());
//
//        listaTarefa.removerTarefa("Tarefa 1");
//        System.out.printf("O número total de elementos na lista é: %d%n", listaTarefa.obterNumeroTotalTarefa());
//
//        listaTarefa.obterDescricaoTarefa();
    }
}